import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MusicService } from '../../services/music.service';

@Component({
  selector: 'app-video-form',
  templateUrl: './video-form.component.html',
  styleUrls: ['./video-form.component.css']
})
export class VideoFormComponent {

  entity:object = {
		name: null,
		video: null
  }

  singers = [];
  albums = [];

  constructor( private music: MusicService ) {
    this.getSingers();
    this.getAlbums();
  }

  
  getSingers() {
    this.music.getSingers()
      .subscribe( singers => {
        this.singers = singers;
      });
  };

  getAlbums() {
    this.music.getAlbums()
      .subscribe( albums => {
        this.albums = albums;
      });
  };


  guardar(videoForm:NgForm){
		console.log('Formulario');
		console.log(this.entity);
	}

}
