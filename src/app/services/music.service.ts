import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(private http: HttpClient) { }

  getQuery(query:string) {
    const url = `https://run.mocky.io/v3/${query}`;
    return this.http.get(url);
  };

  getSingers() {
    return this.getQuery('4806d16f-2ed6-40d6-9250-35d6a41ac9b1')
      .pipe(map(data => data));
  };

  getAlbums() {
    return this.getQuery('4caa781f-8831-41a8-8bc3-9c668d8d66bf')
      .pipe(map(data => data));
  };

}
